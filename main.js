"use strict"

// Реалізувати програму, яка циклічно показує різні картинки.

let images = document.querySelectorAll('.images-wrapper img');

images.forEach( (image) => image.hidden = true );

// При запуску програми на екрані має відображатись перша картинка
// Через 3 секунди замість неї має бути показано друга картинка

let imageIndex = 0;
let seconds = 1000;

// При запуску програми картинки змінюються
let isAnimating = true;

function showImage() {
    images.forEach( (image) => image.hidden = true );
    images[imageIndex].hidden = false;
    images[imageIndex].style.margin = '0 auto';
    imageIndex++;

    if (imageIndex < images.length) {
        imageTimeout = setTimeout(showImage, seconds);
    }
    // Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
    else {
        imageIndex = 0;
        imageTimeout = setTimeout(showImage, seconds);
    }
}

let imageTimeout = setTimeout(showImage);

// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.

// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною 
// та картинка, яка була там при натисканні кнопки.

let buttonsWrapper = document.querySelector('.buttons-wrapper');
let stopButton = document.createElement('button');
stopButton.textContent = 'Stop';
stopButton.classList.add('stop-button');
buttonsWrapper.append(stopButton);

stopButton.addEventListener('click', function () {
    console.log(imageIndex);
    clearTimeout(imageTimeout);

    // При натисненні кнопки "Стоп" цикл зміни картинок завершується
    isAnimating = false;
})

let resumeButton = document.createElement('button');
resumeButton.textContent = 'Resume';
resumeButton.classList.add('resume-button');
buttonsWrapper.append(resumeButton);

resumeButton.addEventListener('click', function () {

    // Якщо при натисненні на кнопку "Продовжити" анімація не відбувається
    // (картинки не змінюються), то лише тоді цикл може продовжитись 
    if (!isAnimating) {           
        imageTimeout = setTimeout(showImage, seconds);
        isAnimating = true;
    }
});

